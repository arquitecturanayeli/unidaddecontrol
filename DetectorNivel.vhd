library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity DetectorNivel is
    Port ( CLK : in  STD_LOGIC;
           CLR : in  STD_LOGIC;
           NA : out  STD_LOGIC);
end DetectorNivel;

architecture Behavioral of DetectorNivel is

signal pclk: std_logic;
signal nclk: std_logic;
SIGNAL Rclr: std_logic;

begin
PCLR: PROCESS(CLK)
BEGIN
	IF(FALLING_EDGE(CLK))THEN
		RCLR <= CLR;
	END IF;
END PROCESS PCLR;

NIVEL: PROCESS(CLK, RCLR, pclk, nclk)
BEGIN	
	IF(RCLR = '1') THEN
		NA <= '0';
		pclk <= '0';
		nclk <= '0';
	ELSIF(RISING_EDGE(CLK))THEN
		pclk <= NOT pclk;
	ELSIF(FALLING_EDGE(CLK))THEN
		nclk <= NOT nclk;
	END IF;
	
	NA <= pclk XOR nclk;
END PROCESS NIVEL;



end Behavioral;

