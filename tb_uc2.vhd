LIBRARY ieee;
LIBRARY STD;
USE STD.TEXTIO.ALL;
USE ieee.std_logic_TEXTIO.ALL;
USE ieee.std_logic_1164.ALL;
USE ieee.std_logic_UNSIGNED.ALL;
USE ieee.std_logic_ARITH.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY tb_uc2 IS
END tb_uc2;
 
ARCHITECTURE behavior OF tb_uc2 IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT Control_principal
    PORT(
         clk : IN  std_logic;
         clr : IN  std_logic;
         lf : IN  std_logic;
         banderas_in : IN  std_logic_vector(3 downto 0);
         OpC : IN  std_logic_vector(4 downto 0);
         cod_func : IN  std_logic_vector(3 downto 0);
         datos_out : OUT  std_logic_vector(19 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal clr : std_logic := '0';
   signal lf : std_logic := '0';
   signal BANDERAS : std_logic_vector(3 downto 0) := (others => '0');
   signal OP_CODE : std_logic_vector(4 downto 0) := (others => '0');
   signal FUN_CODE : std_logic_vector(3 downto 0) := (others => '0');

 	--Outputs
   signal MICROINSTRUCCION : std_logic_vector(19 downto 0);

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: Control_principal PORT MAP (
          clk => clk,
          clr => clr,
          lf => lf,
          banderas_in => BANDERAS,
          OpC => OP_CODE,
          cod_func => FUN_CODE,
          datos_out => MICROINSTRUCCION
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

 -- Stimulus process
   stim_proc: process
   file ARCH_OUT : TEXT;																					
	variable LINEA_OUT : line;
	variable VAR_CLR : STD_LOGIC;
	variable VAR_LF : STD_LOGIC;
	variable VAR_OP_CODE : STD_LOGIC_VECTOR (4 downto 0);
	variable VAR_FUN_CODE : STD_LOGIC_VECTOR (3 downto 0);
	variable VAR_BANDERAS : STD_LOGIC_VECTOR (3 downto 0);

	file ARCH_IN : TEXT;
	variable LINEA_IN : line;
	variable VAR_MICROINSTRUCCION : STD_LOGIC_VECTOR (19 downto 0);
	
	variable CADENA : STRING(1 TO 8);
	variable CADENA1 : STRING(1 TO 20);
	variable CADENA2 : STRING(1 TO 3);
	
	begin
		
		file_open(ARCH_IN, "./Entradas.txt", READ_MODE); 	
		file_open(ARCH_OUT, "./Respuestas.txt", WRITE_MODE); 	
			
		CADENA := " OP_CODE";
		write(LINEA_OUT, CADENA, right, CADENA'LENGTH+1);
		CADENA := "FUN_CODE";
		write(LINEA_OUT, CADENA, right, CADENA'LENGTH+1);
		CADENA := "BANDERAS";
		write(LINEA_OUT, CADENA, right, CADENA'LENGTH+1);
		CADENA2 := "CLR";
		write(LINEA_OUT, CADENA2, right, CADENA2'LENGTH+1);
		CADENA2 := " LF";
		write(LINEA_OUT, CADENA2, right, CADENA2'LENGTH+1);
		CADENA1 := "    MICROINSTRUCCION";
		write(LINEA_OUT, CADENA1, right, CADENA1'LENGTH+1);
		CADENA := "   NIVEL";
		write(LINEA_OUT, CADENA, right, CADENA'LENGTH+1);
		writeline(ARCH_OUT,LINEA_OUT);
		
		--Instrucciones Tipo R
		for I in 0 to 15 loop
			readline(ARCH_IN,LINEA_IN);
			
			read(LINEA_IN, VAR_OP_CODE);
			OP_CODE<= VAR_OP_CODE;
			read(LINEA_IN, VAR_FUN_CODE);
			FUN_CODE<= VAR_FUN_CODE;
			read(LINEA_IN, VAR_BANDERAS);
			BANDERAS<= VAR_BANDERAS;
			read(LINEA_IN, VAR_CLR);
			CLR<= VAR_CLR;
			read(LINEA_IN, VAR_LF);
			LF<= VAR_LF;
			
			wait for 5 ns;
			
			VAR_MICROINSTRUCCION:= MICROINSTRUCCION;
			
			write(LINEA_OUT,VAR_OP_CODE,right,9);
			write(LINEA_OUT,VAR_FUN_CODE,right,9);
			write(LINEA_OUT,VAR_BANDERAS,right,9);
			write(LINEA_OUT,VAR_CLR,right,4);
			write(LINEA_OUT,VAR_LF,right,4);
			write(LINEA_OUT,VAR_MICROINSTRUCCION,right,21);
			if(CLK = '1') then
				CADENA := "    ALTO";
				write(LINEA_OUT, CADENA, right, CADENA'LENGTH+1);
			else
				CADENA := "    BAJO";
				write(LINEA_OUT, CADENA, right, CADENA'LENGTH+1);
			end if;
			
			writeline(ARCH_OUT,LINEA_OUT);
		end loop;
		
		--Intrucciones Tipo I y J
		for I in 0 to 12 loop
			readline(ARCH_IN,LINEA_IN);
			
			read(LINEA_IN, VAR_OP_CODE);
			OP_CODE<= VAR_OP_CODE;
			read(LINEA_IN, VAR_FUN_CODE);
			FUN_CODE<= VAR_FUN_CODE;
			read(LINEA_IN, VAR_BANDERAS);
			BANDERAS<= VAR_BANDERAS;
			read(LINEA_IN, VAR_CLR);
			CLR<= VAR_CLR;
			read(LINEA_IN, VAR_LF);
			LF<= VAR_LF;
			
			wait for 5 ns;
			
			VAR_MICROINSTRUCCION:= MICROINSTRUCCION;
			
			write(LINEA_OUT,VAR_OP_CODE,right,9);
			write(LINEA_OUT,VAR_FUN_CODE,right,9);
			write(LINEA_OUT,VAR_BANDERAS,right,9);
			write(LINEA_OUT,VAR_CLR,right,4);
			write(LINEA_OUT,VAR_LF,right,4);
			write(LINEA_OUT,VAR_MICROINSTRUCCION,right,21);
			if(CLK = '1') then
				CADENA := "    ALTO";
				write(LINEA_OUT, CADENA, right, CADENA'LENGTH+1);
			else
				CADENA := "    BAJO";
				write(LINEA_OUT, CADENA, right, CADENA'LENGTH+1);
			end if;
			
			writeline(ARCH_OUT,LINEA_OUT);
		end loop;
		
		--Intrucciones de Brinco Condicional
		for I in 0 to 17 loop
			readline(ARCH_IN,LINEA_IN);
			
			read(LINEA_IN, VAR_OP_CODE);
			OP_CODE<= VAR_OP_CODE;
			read(LINEA_IN, VAR_FUN_CODE);
			FUN_CODE<= VAR_FUN_CODE;
			read(LINEA_IN, VAR_BANDERAS);
			BANDERAS<= VAR_BANDERAS;
			read(LINEA_IN, VAR_CLR);
			CLR<= VAR_CLR;
			read(LINEA_IN, VAR_LF);
			LF<= VAR_LF;
			
			wait for 5 ns;
			
			VAR_MICROINSTRUCCION:= MICROINSTRUCCION;
			
			write(LINEA_OUT,VAR_OP_CODE,right,9);
			write(LINEA_OUT,VAR_FUN_CODE,right,9);
			write(LINEA_OUT,VAR_BANDERAS,right,9);
			write(LINEA_OUT,VAR_CLR,right,4);
			write(LINEA_OUT,VAR_LF,right,4);
			write(LINEA_OUT,VAR_MICROINSTRUCCION,right,21);
			if(CLK = '1') then
				CADENA := "    ALTO";
				write(LINEA_OUT, CADENA, right, CADENA'LENGTH+1);
			else
				CADENA := "    BAJO";
				write(LINEA_OUT, CADENA, right, CADENA'LENGTH+1);
			end if;
			
			writeline(ARCH_OUT,LINEA_OUT);
		end loop;
		
		--Intrucciones de Brinco Incondicional y Manejo de Subrutinas
		for I in 0 to 4 loop
			readline(ARCH_IN,LINEA_IN);
			
			read(LINEA_IN, VAR_OP_CODE);
			OP_CODE<= VAR_OP_CODE;
			read(LINEA_IN, VAR_FUN_CODE);
			FUN_CODE<= VAR_FUN_CODE;
			read(LINEA_IN, VAR_BANDERAS);
			BANDERAS<= VAR_BANDERAS;
			read(LINEA_IN, VAR_CLR);
			CLR<= VAR_CLR;
			read(LINEA_IN, VAR_LF);
			LF<= VAR_LF;
			
			wait for 5 ns;
			
			VAR_MICROINSTRUCCION:= MICROINSTRUCCION;
			
			write(LINEA_OUT,VAR_OP_CODE,right,9);
			write(LINEA_OUT,VAR_FUN_CODE,right,9);
			write(LINEA_OUT,VAR_BANDERAS,right,9);
			write(LINEA_OUT,VAR_CLR,right,4);
			write(LINEA_OUT,VAR_LF,right,4);
			write(LINEA_OUT,VAR_MICROINSTRUCCION,right,21);
			if(CLK = '1') then
				CADENA := "    ALTO";
				write(LINEA_OUT, CADENA, right, CADENA'LENGTH+1);
			else
				CADENA := "    BAJO";
				write(LINEA_OUT, CADENA, right, CADENA'LENGTH+1);
			end if;
			
			writeline(ARCH_OUT,LINEA_OUT);
		end loop;
		
		file_close(ARCH_IN);
		file_close(ARCH_OUT);

      wait;
   end process;

END;
