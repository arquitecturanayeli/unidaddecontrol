library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Registro is
    Port ( lf : in  STD_LOGIC;
           clk : in  STD_LOGIC;
           clr : in  STD_LOGIC;
           banderas_in : in  STD_LOGIC_VECTOR (3 downto 0);
           banderas_out : out  STD_LOGIC_VECTOR (3 downto 0));
end Registro;

architecture Behavioral of Registro is

begin
	process(clr,clk)
		begin
			if(clr = '1')then
				banderas_out <= "0000";
			elsif(falling_edge(clk))then--cuando esta en el flanco de bajada el reloj entonces lf habilita la carga del registro
				if(lf='1')then
					banderas_out <= banderas_in;
				end if;
			end if;
		end process;

end Behavioral;

