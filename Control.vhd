library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Control is
    Port ( clk : in  STD_LOGIC;
			  clr : in  STD_LOGIC;
			  --=====================
			  --DECO
           TIPOR : in  STD_LOGIC;
           BEQ : in  STD_LOGIC;
           BNEQ : in  STD_LOGIC;
           BLT : in  STD_LOGIC;
           BLE : in  STD_LOGIC;
           BGT : in  STD_LOGIC;
           BGET : in  STD_LOGIC;
			  --====================
			  --Condicion
           EQ : in  STD_LOGIC;
           NEQ : in  STD_LOGIC;
           LT : in  STD_LOGIC;
           LE : in  STD_LOGIC;
           G : in  STD_LOGIC;
           GET : in  STD_LOGIC;
			  --===================
			  --Unidad de control
			  SDOPC : out  STD_LOGIC;
           SM : out  STD_LOGIC;
			  --===================
			  -- Detector de Nivel
			  NA: in STD_LOGIC
			  );
			  
end Control;

architecture Behavioral of Control is

type estados is (A);
signal edo_actual, edo_sig : estados;

begin

	process (clk, clr)
	begin
		if (clr = '1') then
			edo_actual <= A;
		elsif (rising_edge(clk)) then
			edo_actual <= edo_sig;
		end if;	
	end process;
	
	process(TIPOR, BEQ, EQ, BNEQ, BLT, BLE, BGT, BGET, NEQ, LT, LE, G, GET, clk, edo_actual) --clk se agrega en lugar de NA
	begin
		
		sdopc <= '0';
		sm <= '0';
		
			case edo_actual is
				when A => edo_sig <= A;
						if (TIPOR = '0') then
							if (BEQ = '1') then	-- pregunta por la salidad del decodificador
								if (NA = '1') then
									sdopc <= '0';
									sm <= '1';
								else
									if (eq = '1') then -- pregunta por la bandera para hacer salto
										sdopc <= '1';
										sm <= '1';
									else
										sdopc <= '0';
										sm <= '1';
									end if;
								end if;
							else
								if (BNEQ = '1') then	-- pregunta por la salidad del decodificador
									if (NA = '1') then
										sdopc <= '0';
										sm <= '1';
									else
										if (neq = '1') then -- pregunta por la bandera para hacer salto
											sdopc <= '1';
											sm <= '1';
										else
											sdopc <= '0';
											sm <= '1';
										end if;
									end if;
								else
									if (BLT = '1') then	-- pregunta por la salidad del decodificador
										if (NA = '1') then
											sdopc <= '0';
											sm <= '1';
										else
											if (lt = '1') then -- pregunta por la bandera para hacer salto
												sdopc <= '1';
												sm <= '1';
											else
												sdopc <= '0';
												sm <= '1';
											end if;
										end if;
									else
										if (BLE = '1') then	-- pregunta por la salidad del decodificador
											if (NA = '1') then
												sdopc <= '0';
												sm <= '1';
											else
												if (le = '1') then -- pregunta por la bandera para hacer salto
													sdopc <= '1';
													sm <= '1';
												else
													sdopc <= '0';
													sm <= '1';
												end if;
											end if;
										else
											if (BGT = '1') then	-- pregunta por la salidad del decodificador
												if (NA = '1') then
													sdopc <= '0';
													sm <= '1';
												else
													if (g = '1') then -- pregunta por la bandera para hacer salto
														sdopc <= '1';
														sm <= '1';
													else
														sdopc <= '0';
														sm <= '1';
													end if;
												end if;
											else
												if (BGET = '1') then	-- pregunta por la salidad del decodificador
													if (NA = '1') then
														sdopc <= '0';
														sm <= '1';
													else
														if (GET = '1') then -- pregunta por la bandera para hacer salto
															sdopc <= '1';
															sm <= '1';
														else
															sdopc <= '0';
															sm <= '1';
														end if;
													end if;
												else
													sm <= '1';
													sdopc <= '1';
												end if;
											end if;
										end if;	
									end if;
								end if;	
							end if;		
						else
							sm <= '0';
							sdopc <= '0';
						end if;
				end case;
	end process;

end Behavioral;