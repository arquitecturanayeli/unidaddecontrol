
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity Opcode is
	Port ( bus_datos : out  STD_LOGIC_VECTOR (19 downto 0);
           bus_dir : in  STD_LOGIC_VECTOR (4 downto 0));
end Opcode;

architecture Behavioral of Opcode is
--son instrucciones que tienen que ver con saltos y literales junto con registros 
type Memoria is array(0 to 31) of std_logic_vector(19 downto 0);
--Microinstruccion UP DW WPC SDMP SR2 SWD SHE DIR WR LF SEXT SOP1 SOP2 ALUOP SDMD WD SR--
constant mem_OP: Memoria:=("0000100001000" & "0111" & "000", --0 Comparacion--
									"0000000010000" & "0000" & "000", --1 LI--
									"0000010010000" & "0000" & "100", --2 LWI--
									"0000100000000" & "0000" & "110", --3 SWI--
									"0000100000101" & "0011" & "010", --4 SW--
									"0000010011001" & "0011" & "001", --5 ADDI--
									"0000010011001" & "0111" & "001", --6 SUBI--
									"0000010011101" & "0000" & "001", --7 ANDI--
									"0000010011101" & "0001" & "001", --8 ORI--
									"0000010011101" & "0010" & "001", --9 XORI--
									"0000010011101" & "1101" & "001", --10 NANDI--
									"0000010011101" & "1100" & "001", --11 NORI--
									"0000010011101" & "1010" & "001", --12 XNORI--
									"0011000000011" & "0011" & "001", --13 BEQI--
									"0011000000011" & "0011" & "001", --14 BNEI--
									"0011000000011" & "0011" & "001", --15 BLTI--
									"0011000000011" & "0011" & "001", --16 BLETI--
									"0011000000011" & "0011" & "001", --17 BGTI--
									"0011000000011" & "0011" & "001", --18 BGETI--
									"0010000000000" & "0000" & "000", --19 B--
									"1010000000000" & "0000" & "000", --20 CALL--
									"0100000000000" & "0000" & "000", --11 RET--
									"0000000000000" & "0000" & "000", --22 NOP--
									"0000010010101" & "0011" & "000", --23 LW--
								others => "00000000000000000000");
									

begin

	process(bus_dir)
		begin
		bus_datos <= mem_OP(conv_integer(bus_dir));
		end process;

end Behavioral;

