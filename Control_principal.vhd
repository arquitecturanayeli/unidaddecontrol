library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.Paquete_control.all;



entity Control_principal is
    Port (  clk : in  STD_LOGIC;
				clr : in STD_LOGIC;
				lf : in STD_LOGIC;
				banderas_in : in STD_LOGIC_VECTOR(3 DOWNTO 0);
				OpC : in STD_LOGIC_VECTOR(4 DOWNTO 0);
				cod_func : in STD_LOGIC_VECTOR(3 DOWNTO 0);
				datos_out : out STD_LOGIC_VECTOR(19 DOWNTO 0)
				);
end Control_principal;

architecture Behavioral of Control_principal is

signal OpC_mux : STD_LOGIC_VECTOR(4 DOWNTO 0);
signal datos_out_OP : STD_LOGIC_VECTOR(19 DOWNTO 0);
signal datos_out_CF : STD_LOGIC_VECTOR(19 DOWNTO 0);
signal banderas_out : STD_LOGIC_VECTOR(3 DOWNTO 0);
signal eq, neq, lt, le, g, get, tipor, beq, bneq, blt, ble,bgt, bget, sdopc, sm, na : std_logic;

begin

-- Creamos instancia de cod_funcion
	CF: Cod_funcion port map
		(
			bus_datos => datos_out_CF,
			bus_dir => cod_func
		);
		
		
	-- Creamos instancia de Opcode
	OP: Opcode port map
		(
			bus_datos => datos_out_OP,
			bus_dir => OpC_mux
		);
	-- Creamos instancia de condicion
	Cond: Condicion port map
		(
			banderas  => banderas_in,
			EQ => eq,
         NEQ => neq,
         LT => lt,
         LE => le,
         G  => g,
         GET => get
		);
	-- Creamos instancia de decodificador
	Deco: Decodificador port map
		(
			bus_dir => OpC,
			TipoR => tipor,
			BEQ => beq,
			BNEQ => bneq,
			BLT => blt,
			BLE => ble,
			BGT => bgt,
			BGET => bget
		);
		
	-- Creamos instancia de registro
	Reg: Registro port map
		(
			lf => lf,
         clk => clk,
         clr => clr,
         banderas_in  => banderas_in,
         banderas_out  => banderas_out
		);
	-- Creamos instancia de control
	Cont: Control port map
		(
         clk => clk,
         clr => clr,
         TipoR => tipor,
			BEQ => beq,
			BNEQ => bneq,
			BLT => blt,
			BLE => ble,
			BGT => bgt,
			BGET => bget,
			EQ => eq,
         NEQ => neq,
         LT => lt,
         LE => le,
         G  => g,
         GET => get,
			SDOPC => sdopc,
 			SM => sm,
			NA=> na 
		);
		
	NIV : DetectorNivel PORT MAP(
		CLK => clk,
		CLR => clr,
		NA => na
	);

--son los muxes que deciden si es de tipo r o algn brinco dependiendo la seal que se active es su resultado
		
mux_SM : process(sm,datos_out_OP,datos_out_CF)
			begin
				if(sm = '1')then--si es uno carga lo que hay en la memoria de opcode
					datos_out <= datos_out_OP;
				else
					datos_out <= datos_out_CF;--si es cero carga habilita el mux para cargar el codigo de funcion
				end if;
			end process mux_SM;
		
mux_SDOPC : process(sdopc,OpC)--este mux recibe el codigo de operacion y decodifica la instruccion
			begin
				if(sdopc = '1')then--si es 1 busca el codigo de operacion en opcode
					OpC_mux <= OpC;
				else
					OpC_mux <= "00000";--si no entonces es una tipo r y la cargamos con ceros 
				end if;
			end process mux_SDOPC;
					

end Behavioral;