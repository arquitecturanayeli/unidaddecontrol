LIBRARY ieee;
library std;
use std.textio.all;
USE ieee.std_logic_1164.ALL;
use ieee.Numeric_std.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_textio.all;
 
 
ENTITY Prueba_control IS
END Prueba_control;
 
ARCHITECTURE behavior OF Prueba_control IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT Control_principal
    PORT(
         clk : IN  std_logic;
         clr : IN  std_logic;
         lf : IN  std_logic;
         banderas_in : IN  std_logic_vector(3 downto 0);
         OpC : IN  std_logic_vector(4 downto 0);
         cod_func : IN  std_logic_vector(3 downto 0);
         datos_out : OUT  std_logic_vector(19 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal clr : std_logic := '0';
   signal lf : std_logic := '0';
   signal banderas_in : std_logic_vector(3 downto 0) := (others => '0');
   signal OpC : std_logic_vector(4 downto 0) := (others => '0');
   signal cod_func : std_logic_vector(3 downto 0) := (others => '0');

 	--Outputs
   signal datos_out : std_logic_vector(19 downto 0);

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: Control_principal PORT MAP (
          clk => clk,
          clr => clr,
          lf => lf,
          banderas_in => banderas_in,
          OpC => OpC,
          cod_func => cod_func,
          datos_out => datos_out
        );
		  
	  
		  
LecEsc :process
		file ARCH_RES : TEXT;																					
		variable LINEA_RES : line;
		VARIABLE VAR_Datos_out : STD_LOGIC_VECTOR(19 DOWNTO 0);
			
		file ARCH_VEC : TEXT;
		variable LINEA_VEC : line;
		VARIABLE VAR_Banderas_in : STD_LOGIC_VECTOR(3 DOWNTO 0);
		VARIABLE VAR_OpC : STD_LOGIC_VECTOR(4 DOWNTO 0);
		VARIABLE VAR_Cod_funcion : STD_LOGIC_VECTOR(3 DOWNTO 0);
		
		VARIABLE VAR_clr : STD_LOGIC;
		VARIABLE VAR_lf : STD_LOGIC;
		VARIABLE CADENA : STRING(1 TO 16);
		VARIABLE CAD : STRING(1 TO 10);
		VARIABLE NIVEL : STRING(1 TO 10);
		
	begin
			file_open(ARCH_VEC, "./Entradas.txt", READ_MODE); 	
			file_open(ARCH_RES, "./Respuestas.txt", WRITE_MODE); 	
		
		CAD := " OP_CODE  ";
		write(LINEA_RES, CAD, right, CAD'LENGTH+1);	
		CAD := " FUN_CODE ";
		write(LINEA_RES, CAD, right, CAD'LENGTH+1);	
		CAD := " BANDERAS ";
		write(LINEA_RES, CAD, right, CAD'LENGTH+1);	
		CAD := "  CLR     ";
		write(LINEA_RES, CAD, right, CAD'LENGTH+1);	
		CAD := " LF       ";
		write(LINEA_RES, CAD, right, CAD'LENGTH+1);	
		CADENA := "MICROINSTRUCCION";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);
		CAD := "     NIVEL";
		write(LINEA_RES, CAD, right, CADENA'LENGTH+1);
	
		
		
		WAIT FOR 15 NS;
		FOR I IN 0 TO 51 LOOP
		
			readline(ARCH_VEC,LINEA_VEC);

			read(LINEA_VEC, VAR_OpC);
			OpC <= VAR_OpC;
			
			read(LINEA_VEC, VAR_Cod_funcion);
			Cod_func <= VAR_Cod_funcion;
			
			read(LINEA_VEC, VAR_Banderas_in);
			Banderas_in <= VAR_Banderas_in;
			
			read(LINEA_VEC, VAR_clr);
			clr <= VAR_clr;
			
			read(LINEA_VEC, VAR_lf);
			lf <= VAR_lf;
			
			
			
				WAIT UNTIL RISING_EDGE(CLK);	
			
			if(lf = '1')then
				NIVEL := "      BAJO";
			else
				NIVEL := "      ALTO";
			end if;
			VAR_Datos_out := Datos_out;	
			
			writeline(ARCH_RES,LINEA_RES);
			
			write(LINEA_RES, VAR_OpC, 	right, 9);
			write(LINEA_RES, VAR_Cod_funcion, 	right, 9);
			write(LINEA_RES, VAR_Banderas_in, 	right, 9);
			write(LINEA_RES, VAR_clr, 	right, 9);
			write(LINEA_RES, VAR_lf, 	right, 9);
			write(LINEA_RES, VAR_Datos_out, right, 30);
			write(LINEA_RES, NIVEL, right, NIVEL'LENGTH+1);

			writeline(ARCH_RES,LINEA_RES);
			
		end loop;
		
		file_close(ARCH_VEC);  
		file_close(ARCH_RES);  

      wait;     
	
	end process LecEsc ;

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

END;