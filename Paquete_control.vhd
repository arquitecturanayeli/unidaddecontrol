

library IEEE;
use IEEE.STD_LOGIC_1164.all;

package Paquete_control is

COMPONENT Cod_funcion is 
	Port ( bus_datos : out  STD_LOGIC_VECTOR (19 downto 0);
           bus_dir : in  STD_LOGIC_VECTOR (3 downto 0));
end COMPONENT;

COMPONENT Condicion is
    Port ( banderas : in  STD_LOGIC_VECTOR (3 downto 0);
			EQ : out  STD_LOGIC;
         NEQ : out  STD_LOGIC;
         LT : out  STD_LOGIC;
         LE : out  STD_LOGIC;
         G : out  STD_LOGIC;
         GET : out  STD_LOGIC);
end COMPONENT;

COMPONENT Control is
    Port ( clk : in  STD_LOGIC;
			  clr : in  STD_LOGIC;
           TIPOR : in  STD_LOGIC;
           BEQ : in  STD_LOGIC;
           BNEQ : in  STD_LOGIC;
           BLT : in  STD_LOGIC;
           BLE : in  STD_LOGIC;
           BGT : in  STD_LOGIC;
           BGET : in  STD_LOGIC;
           EQ : in  STD_LOGIC;
           NEQ : in  STD_LOGIC;
           LT : in  STD_LOGIC;
           LE : in  STD_LOGIC;
           G : in  STD_LOGIC;
           GET : in  STD_LOGIC;
			  NA : in STD_LOGIC;
			  SDOPC : out  STD_LOGIC;
           SM : out  STD_LOGIC
			  );
			  
end COMPONENT;

COMPONENT Decodificador is
    Port ( bus_dir : in  STD_LOGIC_VECTOR (4 downto 0);
			TipoR: out std_logic;
			BEQ: out std_logic;
			BNEQ: out std_logic;
			BLT: out std_logic;
			BLE: out std_logic;
			BGT: out std_logic;
			BGET: out std_logic
			);
end COMPONENT;

COMPONENT Opcode is
	Port ( bus_datos : out  STD_LOGIC_VECTOR (19 downto 0);
           bus_dir : in  STD_LOGIC_VECTOR (4 downto 0));
end COMPONENT;

COMPONENT Registro is
    Port ( lf : in  STD_LOGIC;
           clk : in  STD_LOGIC;
           clr : in  STD_LOGIC;
           banderas_in : in  STD_LOGIC_VECTOR (3 downto 0);
           banderas_out : out  STD_LOGIC_VECTOR (3 downto 0));
end COMPONENT;

COMPONENT DetectorNivel is
    Port ( CLK : in  STD_LOGIC;
           CLR : in  STD_LOGIC;
           NA : out  STD_LOGIC);
end COMPONENT;

end Paquete_control;

package body Paquete_control is

---- Example 1
--  function <function_name>  (signal <signal_name> : in <type_declaration>  ) return <type_declaration> is
--    variable <variable_name>     : <type_declaration>;
--  begin
--    <variable_name> := <signal_name> xor <signal_name>;
--    return <variable_name>; 
--  end <function_name>;

---- Example 2
--  function <function_name>  (signal <signal_name> : in <type_declaration>;
--                         signal <signal_name>   : in <type_declaration>  ) return <type_declaration> is
--  begin
--    if (<signal_name> = '1') then
--      return <signal_name>;
--    else
--      return 'Z';
--    end if;
--  end <function_name>;

---- Procedure Example
--  procedure <procedure_name>  (<type_declaration> <constant_name>  : in <type_declaration>) is
--    
--  begin
--    
--  end <procedure_name>;
 
end Paquete_control;
