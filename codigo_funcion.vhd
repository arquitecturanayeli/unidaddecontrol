library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity Cod_funcion is
    Port ( bus_datos : out  STD_LOGIC_VECTOR (19 downto 0);
           bus_dir : in  STD_LOGIC_VECTOR (3 downto 0));
end Cod_funcion;

architecture Behavioral of Cod_funcion is
--saca las miscroinstrucciones esta es la memoria de funcion son instrucciones que solo tienen que ver con registros
type Memoria is array(0 to 15) of std_logic_vector(19 downto 0);
--Microinstruccion UP DW WPC SDMP SR2 SWD SHE DIR WR LF SEXT SOP1 SOP2 ALUOP SDMD WD SR--
constant funcion: Memoria:=("0000010011000" & "0011" & "001", --ADD--
									"0000010011000" & "0111" & "001", --SUB--
									"0000010011000" & "0000" & "001", --AND--
									"0000010011000" & "0001" & "001", --OR--
									"0000010011000" & "0010" & "001", --XOR--
									"0000010011000" & "1101" & "001", --NAND--
									"0000010011000" & "1100" & "001", --NOR--
									"0000010011000" & "1010" & "001", --XNOR--
									"0000010011000" & "1101" & "001", --NOT--
									"0000001100000" & "0000" & "000", --SLL--
									"0000001000000" & "0000" & "000", --SRL--
								others => "00000000000000000000");

begin

process(bus_dir)
	begin
	bus_datos <= funcion(conv_integer(bus_dir));--le da al bus de datos la microinstruccion que esta en la direccion
															  --bus_dir
	end process;

end Behavioral;

