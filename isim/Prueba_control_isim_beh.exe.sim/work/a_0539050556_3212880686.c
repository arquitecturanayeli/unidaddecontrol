/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0xfbc00daa */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "/home/juancarlos/Documentos/escom/7moSemestre/arquitectura/3Parcial/UCfix/unidaddecontrol/Decodificador.vhd";



static void work_a_0539050556_3212880686_p_0(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    int t4;
    char *t5;
    char *t6;
    int t7;
    char *t8;
    int t10;
    char *t11;
    int t13;
    char *t14;
    int t16;
    char *t17;
    int t19;
    char *t20;
    int t22;
    char *t23;
    char *t24;
    char *t25;
    char *t26;
    char *t27;

LAB0:    xsi_set_current_line(25, ng0);
    t1 = (t0 + 1032U);
    t2 = *((char **)t1);
    t1 = (t0 + 6597);
    t4 = xsi_mem_cmp(t1, t2, 5U);
    if (t4 == 1)
        goto LAB3;

LAB11:    t5 = (t0 + 6602);
    t7 = xsi_mem_cmp(t5, t2, 5U);
    if (t7 == 1)
        goto LAB4;

LAB12:    t8 = (t0 + 6607);
    t10 = xsi_mem_cmp(t8, t2, 5U);
    if (t10 == 1)
        goto LAB5;

LAB13:    t11 = (t0 + 6612);
    t13 = xsi_mem_cmp(t11, t2, 5U);
    if (t13 == 1)
        goto LAB6;

LAB14:    t14 = (t0 + 6617);
    t16 = xsi_mem_cmp(t14, t2, 5U);
    if (t16 == 1)
        goto LAB7;

LAB15:    t17 = (t0 + 6622);
    t19 = xsi_mem_cmp(t17, t2, 5U);
    if (t19 == 1)
        goto LAB8;

LAB16:    t20 = (t0 + 6627);
    t22 = xsi_mem_cmp(t20, t2, 5U);
    if (t22 == 1)
        goto LAB9;

LAB17:
LAB10:    xsi_set_current_line(33, ng0);
    t1 = (t0 + 3704);
    t2 = (t1 + 56U);
    t3 = *((char **)t2);
    t5 = (t3 + 56U);
    t6 = *((char **)t5);
    *((unsigned char *)t6) = (unsigned char)2;
    xsi_driver_first_trans_fast_port(t1);

LAB2:    t1 = (t0 + 3624);
    *((int *)t1) = 1;

LAB1:    return;
LAB3:    xsi_set_current_line(26, ng0);
    t23 = (t0 + 3704);
    t24 = (t23 + 56U);
    t25 = *((char **)t24);
    t26 = (t25 + 56U);
    t27 = *((char **)t26);
    *((unsigned char *)t27) = (unsigned char)3;
    xsi_driver_first_trans_fast_port(t23);
    goto LAB2;

LAB4:    xsi_set_current_line(27, ng0);
    t1 = (t0 + 3768);
    t2 = (t1 + 56U);
    t3 = *((char **)t2);
    t5 = (t3 + 56U);
    t6 = *((char **)t5);
    *((unsigned char *)t6) = (unsigned char)3;
    xsi_driver_first_trans_fast_port(t1);
    goto LAB2;

LAB5:    xsi_set_current_line(28, ng0);
    t1 = (t0 + 3832);
    t2 = (t1 + 56U);
    t3 = *((char **)t2);
    t5 = (t3 + 56U);
    t6 = *((char **)t5);
    *((unsigned char *)t6) = (unsigned char)3;
    xsi_driver_first_trans_fast_port(t1);
    goto LAB2;

LAB6:    xsi_set_current_line(29, ng0);
    t1 = (t0 + 3896);
    t2 = (t1 + 56U);
    t3 = *((char **)t2);
    t5 = (t3 + 56U);
    t6 = *((char **)t5);
    *((unsigned char *)t6) = (unsigned char)3;
    xsi_driver_first_trans_fast_port(t1);
    goto LAB2;

LAB7:    xsi_set_current_line(30, ng0);
    t1 = (t0 + 3960);
    t2 = (t1 + 56U);
    t3 = *((char **)t2);
    t5 = (t3 + 56U);
    t6 = *((char **)t5);
    *((unsigned char *)t6) = (unsigned char)3;
    xsi_driver_first_trans_fast_port(t1);
    goto LAB2;

LAB8:    xsi_set_current_line(31, ng0);
    t1 = (t0 + 4024);
    t2 = (t1 + 56U);
    t3 = *((char **)t2);
    t5 = (t3 + 56U);
    t6 = *((char **)t5);
    *((unsigned char *)t6) = (unsigned char)3;
    xsi_driver_first_trans_fast_port(t1);
    goto LAB2;

LAB9:    xsi_set_current_line(32, ng0);
    t1 = (t0 + 4088);
    t2 = (t1 + 56U);
    t3 = *((char **)t2);
    t5 = (t3 + 56U);
    t6 = *((char **)t5);
    *((unsigned char *)t6) = (unsigned char)3;
    xsi_driver_first_trans_fast_port(t1);
    goto LAB2;

LAB18:;
}


extern void work_a_0539050556_3212880686_init()
{
	static char *pe[] = {(void *)work_a_0539050556_3212880686_p_0};
	xsi_register_didat("work_a_0539050556_3212880686", "isim/Prueba_control_isim_beh.exe.sim/work/a_0539050556_3212880686.didat");
	xsi_register_executes(pe);
}
