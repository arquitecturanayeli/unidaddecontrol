
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Decodificador is
    Port ( bus_dir : in  STD_LOGIC_VECTOR (4 downto 0);
			TipoR: out std_logic;
			BEQ: out std_logic;
			BNEQ: out std_logic;
			BLT: out std_logic;
			BLE: out std_logic;
			BGT: out std_logic;
			BGET: out std_logic
			);
end Decodificador;
--Decodificador de la instrucci�n una vez que llega la instrucci�n la decodifica y determina de que tipo es si es de tipo r
--entonces el codigo de operaci�n se llena de ceros si no entonces si no entonces determina que tipo de salto es 
--o el tipo de instruccion 
architecture Behavioral of Decodificador is

begin
--de toda la instrucci�n �nicamente recibe el c�digo de operaci�n por eso son 5 bits
 process(bus_dir) 
	begin
		CASE bus_dir is
			when "00000" =>  TipoR <= '1';
			when "01101" =>  BEQ  <= '1';
			when "01110" =>  BNEQ <= '1';
			when "01111" =>  BLT  <= '1';
			when "10000" =>  BLE  <= '1';
			when "10001" =>  BGT  <= '1';
			when "10010" =>  BGET <= '1';
			when others => TipoR  <= '0';
		end case;							
	end process;

end Behavioral;

