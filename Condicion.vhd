
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity Condicion is
  Port ( banderas : in  STD_LOGIC_VECTOR (3 downto 0);--banderas para determinar si la verificacion se cumpli� o no
			EQ : out  STD_LOGIC;
         NEQ : out  STD_LOGIC;
         LT : out  STD_LOGIC;
         LE : out  STD_LOGIC;
         G : out  STD_LOGIC;
         GET : out  STD_LOGIC);
end Condicion;

architecture Behavioral of Condicion is

begin
--este modulo de condicion determina la se�al de la instruccion de condicion que se llam�
--formulas para determinar que salto se us� casi todas usan a z como 1
EQ<= banderas(0);
	NEQ<= NOT banderas (0);
	LT <= (banderas(1) XOR banderas(2)) AND NOT(banderas(0));
	LE <= (banderas(1) XOR banderas(2)) OR banderas(0);
	G <= (NOT(banderas(1) XOR banderas(2))) AND (NOT banderas(0));
	GET <= (NOT(banderas(1) XOR banderas(2))) OR  banderas(0);

end Behavioral;

